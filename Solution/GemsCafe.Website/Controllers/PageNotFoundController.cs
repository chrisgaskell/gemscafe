﻿namespace GemsCafe.Website.Controllers
{
    using System.Web.Mvc;
    using GemsCafe.Website.App.CmsDefinitions;
    using GemsCafe.Website.ViewModels;
    using Umbraco.Web;
    using Umbraco.Web.Models;

    public class PageNotFoundController : BaseController
    {
        [HttpGet]
        public override ActionResult Index(RenderModel model)
        {
            var viewModel = CreateViewModel<PageNotFoundViewModel>();

            viewModel.Title = CurrentPage.GetPropertyValue<string>(PageNotFoundDoctype.Fields.Title);
            viewModel.Body = CurrentPage.GetPropertyValue<string>(PageNotFoundDoctype.Fields.Body);

            return CurrentTemplate(viewModel);
        }
    }
}