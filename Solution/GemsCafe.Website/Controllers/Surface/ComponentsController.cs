﻿namespace GemsCafe.Website.Controllers.Surface
{
    using System.Web.Mvc;
    using GemsCafe.Website.App;
    using GemsCafe.Website.App.CmsDefinitions;
    using GemsCafe.Website.App.Models;
    using GemsCafe.Website.ViewModels.Components;
    using Umbraco.Web;
    using Umbraco.Web.Mvc;

    public class ComponentsController : SurfaceController
    {
        /*
         * For info - [ChildActionOnly] shouldn't be used with [HttpGet]. 
         * The reason is because when you mark an action with [ChildActionOnly] you are telling MVC to make this not publicly 
         * routable (i.e. it doesn't have a URL) and can only be rendered using Html.Action so applying [HttpGet] is irrelavent.
         */

        [ChildActionOnly]
        public ActionResult TopNavigation()
        {
            var queryService = new QueryService(Umbraco);
            var model = new TopNavigationViewModel();

            // Add home node
            var rootNode = queryService.GetRoot();
            model.NavigationItems.Add(new NavigationItem
            {
                Title = rootNode.Name,
                Url = rootNode.Url
            });

            // Add nodes at first level that havent been hidden using umbracoNaviHide
            foreach (var firstLevelNode in queryService.GetFirstLevelNodes())
            {
                // Check the node isnt hidden from nav
                if (!firstLevelNode.IsVisible()) continue;

                model.NavigationItems.Add(new NavigationItem
                {
                    Title = firstLevelNode.Name,
                    Url = firstLevelNode.Url
                });
            }

            return PartialView(model);
        }

        [ChildActionOnly]
        public ActionResult Header()
        {
            var model = new HeaderViewModel();
            model.Title = CurrentPage.GetPropertyValue<string>(PageDoctype.Fields.HeaderTitle, true);
            model.Address = CurrentPage.GetPropertyValue<string>(PageDoctype.Fields.HeaderAddress, true);
            return PartialView(model);
        }

        [ChildActionOnly]
        public ActionResult Footer()
        {
            var model = new FooterViewModel();
            model.Text = CurrentPage.GetPropertyValue<string>(PageDoctype.Fields.FooterText, true);
            return PartialView(model);
        }
    }
}