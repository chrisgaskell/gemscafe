﻿namespace GemsCafe.Website.Controllers
{
    using System.Web.Mvc;
    using DevTrends.MvcDonutCaching;
    using GemsCafe.Website.App;
    using GemsCafe.Website.App.CmsDefinitions;
    using GemsCafe.Website.App.Helpers;
    using GemsCafe.Website.ViewModels;
    using Umbraco.Web;
    using Umbraco.Web.Models;

    public class AboutController : BaseController
    {
        [DonutOutputCache(CacheProfile = CacheProfiles.SixHours)]
        [HttpGet]
        public override ActionResult Index(RenderModel model)
        {
            var viewModel = CreateViewModel<AboutViewModel>();
            viewModel.Title = CurrentPage.GetPropertyValue<string>(AboutDoctype.Fields.Title);
            viewModel.Intro = CurrentPage.GetPropertyValue<string>(AboutDoctype.Fields.Intro);
            viewModel.IntroImage = CurrentPage.GetImage(Umbraco, AboutDoctype.Fields.IntroImage);
            viewModel.TeamTitle = CurrentPage.GetPropertyValue<string>(AboutDoctype.Fields.TeamTitle);

            // Assemble team members from child nodes
            var queryService = new QueryService(Umbraco);
            var teamMemberNodes = queryService.GetChildNodes(TeamMemberDoctype.DoctypeAlias);

            foreach (var teamMemberNode in teamMemberNodes)
            {
                viewModel.TeamMembers.Add(new TeamMemberViewModel
                {
                    Name = teamMemberNode.GetPropertyValue<string>(TeamMemberDoctype.Fields.Name),
                    JobTitle = teamMemberNode.GetPropertyValue<string>(TeamMemberDoctype.Fields.JobTitle),
                    Image = teamMemberNode.GetImage(Umbraco, TeamMemberDoctype.Fields.Image)
                });
            }

            return CurrentTemplate(viewModel);
        }
    }
}