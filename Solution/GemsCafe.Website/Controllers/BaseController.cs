﻿namespace GemsCafe.Website.Controllers
{
    using System;
    using System.Web.Mvc;
    using GemsCafe.Website.App.CmsDefinitions;
    using GemsCafe.Website.ViewModels.Base;
    using Umbraco.Web;
    using Umbraco.Web.Models;
    using Umbraco.Web.Mvc;

    public abstract class BaseController : RenderMvcController
    {

        // Force the index action to be implemented
        public abstract override ActionResult Index(RenderModel model);

        // Helper to create view model and set the global fields
        protected T CreateViewModel<T>() where T : IBaseViewModel, new()
        {
            var viewModel = new T();
            viewModel.PageTitle = CurrentPage.GetPropertyValue(PageDoctype.Fields.PageTitle, true, "Gems Cafe");
            viewModel.PageDescription = CurrentPage.GetPropertyValue(PageDoctype.Fields.PageDescription, true, "Gems Cafe");
            viewModel.PageConstructedAt = DateTime.Now;
            return viewModel;
        }

    }
}