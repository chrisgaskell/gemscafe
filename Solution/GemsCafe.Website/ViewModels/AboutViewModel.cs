﻿namespace GemsCafe.Website.ViewModels
{
    using System.Collections.Generic;
    using GemsCafe.Website.App.Models;
    using GemsCafe.Website.ViewModels.Base;

    public class AboutViewModel : BaseViewModel
    {
        public AboutViewModel()
        {
            TeamMembers = new List<TeamMemberViewModel>();
        }

        public string Title { get; set; }
        public string Intro { get; set; }
        public string TeamTitle { get; set; }
        public UmbracoImage IntroImage { get; set; }

        public List<TeamMemberViewModel> TeamMembers { get; set; }
    }
}