﻿namespace GemsCafe.Website.ViewModels
{
    using GemsCafe.Website.ViewModels.Base;

    public class PageNotFoundViewModel : BaseViewModel
    {
        public string Title { get; set; }
        public string Body { get; set; }
    }
}