﻿namespace GemsCafe.Website.ViewModels.Base
{
    using System;

    public interface IBaseViewModel
    {
        string PageTitle { get; set; }
        string PageDescription { get; set; }

        // To demonstrate the output caching
        DateTime PageConstructedAt { get; set; }
    }
}