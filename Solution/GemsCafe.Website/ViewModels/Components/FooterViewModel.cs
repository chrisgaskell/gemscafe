﻿namespace GemsCafe.Website.ViewModels.Components
{
    public class FooterViewModel
    {
        public string Text { get; set; }
    }
}