﻿namespace GemsCafe.Website.ViewModels.Components
{
    using System.Collections.Generic;
    using GemsCafe.Website.App.Models;

    public class TopNavigationViewModel
    {
        public TopNavigationViewModel()
        {
            NavigationItems = new List<NavigationItem>();
        }
        public List<NavigationItem> NavigationItems { get; set; }
    }
}