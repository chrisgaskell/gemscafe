﻿namespace GemsCafe.Website.ViewModels.Components
{
    public class HeaderViewModel
    {
        public string Title { get; set; }
        public string Address { get; set; }
    }
}