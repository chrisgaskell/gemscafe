﻿namespace GemsCafe.Website.App.Models
{
    public class NavigationItem
    {
        public string Title { get; set; }
        public string Url { get; set; }
    }
}