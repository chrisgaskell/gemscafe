﻿namespace GemsCafe.Website.App.Models
{
    public class UmbracoImage
    {
        public string Url { get; set; }
        public string Alt { get; set; }
    }
}