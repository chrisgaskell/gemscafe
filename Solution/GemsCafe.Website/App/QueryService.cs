﻿namespace GemsCafe.Website.App
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using log4net;
    using Umbraco.Core.Models;
    using Umbraco.Web;

    public class QueryService
    {
        private readonly ILog _log;
        private readonly UmbracoHelper _umbracoHelper;

        // @CGASKELL:   Umbraco objects can be found in SurfaceController and RenderMvcController
        //              In larger projects you may want to manage this through IoC and perhaps move into a different assembly.
        public QueryService(UmbracoHelper umbracoHelper)
        {
            if (umbracoHelper == null) throw new ArgumentNullException("umbracoHelper");

            _umbracoHelper = umbracoHelper;
            _log = LogManager.GetLogger(GetType());
        }

        public IPublishedContent GetRoot()
        {
            try
            {
                return _umbracoHelper.TypedContentAtRoot().First();
            }
            catch (Exception ex)
            {
                _log.Error("GetRoot()", ex);
            }

            return null;
        }

        public IEnumerable<IPublishedContent> GetFirstLevelNodes()
        {
            try
            {
                return GetRoot().Children;
            }
            catch (Exception ex)
            {
                _log.Error("GetFirstLevelNodes()", ex);
            }

            return new List<IPublishedContent>();
        }

        public IEnumerable<IPublishedContent> GetChildNodes(string doctypeAlias)
        {
            try
            {
                return _umbracoHelper.AssignedContentItem.Children().Where(x => x.DocumentTypeAlias == doctypeAlias);
            }
            catch (Exception ex)
            {
                _log.Error("GetChildNodes(string doctypeAlias)", ex);
            }

            return new List<IPublishedContent>();
        }
    }
}