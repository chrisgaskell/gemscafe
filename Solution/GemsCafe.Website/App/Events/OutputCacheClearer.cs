﻿namespace GemsCafe.Website.App.Events
{
    using System;
    using DevTrends.MvcDonutCaching;
    using umbraco;
    using umbraco.BusinessLogic;
    using umbraco.cms.businesslogic;
    using umbraco.cms.businesslogic.web;
    using Umbraco.Core;
    using Umbraco.Core.Events;
    using Umbraco.Core.Models;
    using Umbraco.Core.Publishing;
    using Umbraco.Core.Services;

    public class OutputCacheClearer : ApplicationEventHandler
    {
        public OutputCacheClearer()
        {
            ContentService.Published += ContentService_Published;
            ContentService.SentToPublish += ContentService_SentToPublish;
            ContentService.Trashed += ContentService_Trashed;
            content.AfterRefreshContent += content_AfterRefreshContent;
        }


        private void ContentService_Published(IPublishingStrategy sender, PublishEventArgs<IContent> e)
        {
            ClearCache();
        }

        private void ContentService_SentToPublish(IContentService sender, SendToPublishEventArgs<IContent> e)
        {
            ClearCache();
        }

        private void ContentService_Trashed(IContentService sender, MoveEventArgs<IContent> e)
        {
            ClearCache();
        }

        private void content_AfterRefreshContent(Document sender, RefreshContentEventArgs e)
        {
            // Happens after "republish entire site" (right click root)
            ClearCache();
        }

        private static void ClearCache()
        {
            try
            {
                //Clear all output cache
                var cacheManager = new OutputCacheManager();
                cacheManager.RemoveItems();
            }
            catch (Exception ex)
            {
                Log.Add(LogTypes.Error, -1, string.Format("Exception: {0} - StackTrace: {1}", ex.Message, ex.StackTrace));
            }
        }
    }
}