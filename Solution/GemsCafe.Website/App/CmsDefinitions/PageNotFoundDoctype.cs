﻿namespace GemsCafe.Website.App.CmsDefinitions
{
    public static class PageNotFoundDoctype
    {
        public static class Fields
        {
            public const string Title = "pagenotfound_title";
            public const string Body = "pagenotfound_body";
        }
    }
}