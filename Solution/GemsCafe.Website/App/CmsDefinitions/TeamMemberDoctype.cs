﻿namespace GemsCafe.Website.App.CmsDefinitions
{
    public static class TeamMemberDoctype
    {
        public const string DoctypeAlias = "TeamMember";

        public static class Fields
        {
            public const string Name = "teammember_name";
            public const string JobTitle = "teammember_jobtitle";
            public const string Image = "teammember_image";
        }
    }
}