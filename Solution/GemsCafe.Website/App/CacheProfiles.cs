﻿namespace GemsCafe.Website.App
{
    public static class CacheProfiles
    {
        // Corresponds with <cache> element in web config
        public const string TwoMins = "TwoMins";
        public const string SixHours = "SixHours";
    }
}