﻿namespace GemsCafe.Website.App.Helpers
{
    using GemsCafe.Website.App.Models;
    using Umbraco.Core.Models;
    using Umbraco.Web;

    public static class PublishedContentHelper
    {
        public static UmbracoImage GetImage(this IPublishedContent content, UmbracoHelper umbraco, string alias)
        {
            var imageId = content.GetPropertyValue(alias, false, 0);
            if (imageId == 0) return null;

            var imageItem = umbraco.TypedMedia(imageId);
            if (imageItem == null) return null;

            return new UmbracoImage
            {
                Alt = imageItem.Name,
                Url = imageItem.Url
            };
        }
    }
}