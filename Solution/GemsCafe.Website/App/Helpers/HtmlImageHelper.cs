﻿namespace GemsCafe.Website.App.Helpers
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web.Mvc;
    using GemsCafe.Website.App.Models;

    public static class HtmlImageHelper
    {
        public static MvcHtmlString UmbracoImage(this HtmlHelper helper,
            UmbracoImage umbracoImage,
            int width = 0,
            int height = 0,
            string css = "")
        {
            if (umbracoImage == null || string.IsNullOrWhiteSpace(umbracoImage.Url))
                return MvcHtmlString.Empty;

            var builder = new TagBuilder("img");
            var imageUrlQs = new List<string>();

            if (!string.IsNullOrWhiteSpace(css)) builder.MergeAttribute("class", css);
            
            if (width > 0)
            {
                builder.MergeAttribute("width", string.Format("{0}px", width));
                imageUrlQs.Add(string.Format("width={0}", width));
            }
            
            if (height > 0)
            {
                builder.MergeAttribute("height", string.Format("{0}px", height));
                imageUrlQs.Add(string.Format("height={0}", height));
            }
            
            // Update Url with qs params if required. Image resizer will pick up the qs values and resize accordingly.
            if (imageUrlQs.Any())
                umbracoImage.Url = string.Format("{0}?{1}", umbracoImage.Url, String.Join("&", imageUrlQs));

            builder.MergeAttribute("src", umbracoImage.Url);
            builder.MergeAttribute("alt", umbracoImage.Alt);

            return MvcHtmlString.Create(builder.ToString(TagRenderMode.SelfClosing));
        }
    }
}